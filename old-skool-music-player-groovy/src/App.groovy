def printHead() {
    // Print the header of the application.
    println("   _____ _     _   _____ _               _  ______ _ ")
    println("  |  _  | |   | | /  ___| |             | | | ___ \\ |")
    println("  | | | | | __| | \\ `--.| | _____   ___ | | | |_/ / | __ _ _   _  ___ _ __")
    println("  | | | | |/ _` |  `--. \\ |/ / _ \\ / _ \\| | |  __/| |/ _` | | | |/ _ \\ '__|")
    println("  \\ \\_/ / | (_| | /\\__/ /   < (_) | (_) | | | |   | | (_| | |_| |  __/ | ")
    println("   \\___/|_|\\__,_| \\____/|_|\\_\\___/ \\___/|_| \\_|   |_|\\__,_|\\__, |\\___|_| ")
    println("                                                            __/ |")
    println("                                                           |___/")

}

def printMenu() {
    // Print the menu of the application.
    println('-------------------------------------------------------------------------')
    println('- Playlist')
    println('-------------------------------------------------------------------------')
    def musicDir = new File('../../music').listFiles().sort()
    def musicDirFiltered = []
    musicDir.each {
        if (it.isDirectory()) {
            musicDirFiltered << it
            println(it.name)
        }
    }
    return musicDirFiltered
}

def choosePlaylist() {
    // Choose which list must play.
    String key = null
    def inputMsg = '\nEnter the playlist number or [0] to quit the application: '
    def allowNumbers = 1..9
    while ( key == null || key != '0' ) {
        def musicDir = printMenu()
        key = System.console().readLine(inputMsg)
        if (key.isNumber()) {
            def n = Integer.parseInt(key)
            if (allowNumbers.contains(n)) {
                def playlistChosen = musicDir[n - 1]
                execPlaylist(playlistChosen)
            }
        }
    }
}

def execPlaylist(dirName) {
    // Exec the playlist chosen.
    listDir = dirName.canonicalPath + '.m3u'
    println('\nPlaying: ' + dirName.name)
    println("Listen for approximately 165 seconds or press [Ctrl + c] to exit the application.")
    def cmd = "mpg123 -q -@ ${listDir}"
    cmd.execute().waitForProcessOutput(System.out, System.err)
}

// starts app
printHead()
choosePlaylist()
