"""This is a very simple music player application implemented in Python."""
import os


MUSIC_PATH = '../music'


def print_head():
    """Print the header of the application."""
    print("   _____ _     _   _____ _               _  ______ _ ")
    print("  |  _  | |   | | /  ___| |             | | | ___ \\ |")
    print("  | | | | | __| | \\ `--.| | _____   ___ | | | |_/ / | __ _ _   _  ___ _ __")
    print("  | | | | |/ _` |  `--. \\ |/ / _ \\ / _ \\| | |  __/| |/ _` | | | |/ _ \\ '__|")
    print("  \\ \\_/ / | (_| | /\\__/ /   < (_) | (_) | | | |   | | (_| | |_| |  __/ | ")
    print("   \\___/|_|\\__,_| \\____/|_|\\_\\___/ \\___/|_| \\_|   |_|\\__,_|\\__, |\\___|_| ")
    print("                                                            __/ |")
    print("                                                           |___/")


def print_menu():
    """Print the menu of the application."""
    print('-------------------------------------------------------------------------')
    print('- Playlist')
    print('-------------------------------------------------------------------------')
    music_dir = sorted(os.listdir(MUSIC_PATH))
    music_dir_filtered = []
    for f in music_dir:
        if os.path.isdir(os.path.join(MUSIC_PATH, f)):
            music_dir_filtered.append(f)
            print(f)
    print('\nEnter the playlist number or [0] to quit the application:')
    return music_dir_filtered


def choose_playlist():
    """Choose which list must play."""
    key = None
    while key is None or key != 0:
        music_dir = print_menu()
        music_len = len(music_dir) + 1
        key = input()
        if key.isnumeric():
            key = int(key)
            if key in range(1, music_len):
                playlist_chosen = music_dir[key - 1]
                exec_play_list(playlist_chosen)


def exec_play_list(dir_name):
    """Exec the playlist chosen."""
    list_dir = MUSIC_PATH + '/' + dir_name + '/*.mp3'
    print('\nPlaying: {}'.format(list_dir))
    print("Keybindings: [f] next [d] previous [q] quit playlist")
    os.system("mpg123 -q {}".format(list_dir))
    print('')


if __name__ == '__main__':
    print_head()
    choose_playlist()
